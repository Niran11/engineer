use std::env;

use crate::{config::Config, error::Error};

#[derive(Debug)]
pub struct Settings {
    config: Config,
}

impl Settings {
    pub fn build() -> Result<Self, Error> {
        let path = env::args().nth(1).ok_or(Error::MissingFilename)?;
        let config = Config::build_from_path(path)?;
        Ok(Self { config })
    }

    pub fn config(&self) -> &Config {
        &self.config
    }
}
