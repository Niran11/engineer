use std::collections::{HashMap, HashSet};

use crate::{
    config::{Build, Deploy},
    error::Error,
    utils::{BuildName, DeployName},
};

pub struct BuildDependencies(HashMap<BuildName, HashSet<BuildName>>);

pub struct DeployDependencies(HashMap<DeployName, HashSet<BuildName>>);

impl BuildDependencies {
    pub fn new(builds: &HashMap<BuildName, Build>) -> Result<Self, Error> {
        let mut me = Self(HashMap::new());
        for (name, build) in builds.iter() {
            let mut deps = HashSet::new();
            for dep in build.dependencies.iter() {
                if me.depends(dep, name) {
                    return Err(Error::CyclomaticDependency(name.clone(), dep.clone()));
                }
                deps.insert(dep.clone());
            }
            me.0.insert(name.clone(), deps);
        }
        Ok(me)
    }

    pub fn depends(&self, build: &BuildName, dependency: &BuildName) -> bool {
        self.depends_directly(build, dependency) || self.depends_indirectly(build, dependency)
    }

    pub fn depends_directly(&self, build: &BuildName, dependency: &BuildName) -> bool {
        self.0
            .get(build)
            .map(|deps| deps.contains(dependency))
            .unwrap_or(false)
    }

    pub fn depends_indirectly(&self, build: &BuildName, dependency: &BuildName) -> bool {
        let mut builds: Vec<_> = self.0.get(build).into_iter().flatten().collect();
        while let Some(build) = builds.pop() {
            if let Some(deps) = self.0.get(build) {
                if deps.contains(dependency) {
                    return true;
                } else {
                    builds.append(&mut deps.iter().collect());
                }
            }
        }
        false
    }

    pub fn all_no_dependencies(&self) -> HashSet<BuildName> {
        self.0
            .iter()
            .filter_map(|(b, deps)| deps.is_empty().then_some(b))
            .cloned()
            .collect()
    }
}

impl DeployDependencies {
    pub fn new(deploys: &HashMap<DeployName, Deploy>) -> Self {
        Self(
            deploys
                .iter()
                .map(|(name, deploy)| (name.clone(), deploy.dependencies.iter().cloned().collect()))
                .collect(),
        )
    }

    pub fn depends(&self, deploy_name: &DeployName, dependency: &BuildName) -> bool {
        self.0
            .get(deploy_name)
            .map(|deps| deps.contains(dependency))
            .unwrap_or(false)
    }

    pub fn has_dependencies(&self, deploy_name: &DeployName) -> bool {
        self.0
            .get(deploy_name)
            .map(|d| !d.is_empty())
            .unwrap_or(false)
    }
}
