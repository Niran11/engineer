use async_trait::async_trait;
use tokio::sync::oneshot;

use crate::{
    builder::{Builder, BuilderMessages},
    error::Error,
};

use super::BuilderState;

pub struct Quitted;

impl Quitted {
    pub(in super::super) fn new_dyn<T>() -> Box<dyn BuilderState<T>>
    where
        T: BuilderMessages + Clone + Sync + Send + 'static,
    {
        Box::new(Self)
    }
}

#[async_trait]
impl<T> BuilderState<T> for Quitted
where
    T: BuilderMessages + Clone + Sync + Send + 'static,
{
    async fn start_build(self: Box<Self>, _builder: &Builder<T>) -> Box<dyn BuilderState<T>> {
        self
    }

    async fn command_completed(
        self: Box<Self>,
        _builder: &Builder<T>,
        _stdout: String,
        _stderr: String,
    ) -> Box<dyn BuilderState<T>> {
        self
    }

    async fn command_failed(
        self: Box<Self>,
        _builder: &Builder<T>,
        _error: Error,
    ) -> Box<dyn BuilderState<T>> {
        self
    }

    async fn next_command(self: Box<Self>, _builder: &Builder<T>) -> Box<dyn BuilderState<T>> {
        self
    }

    async fn quit(
        self: Box<Self>,
        _builder: &Builder<T>,
        _sender: oneshot::Sender<()>,
    ) -> Box<dyn BuilderState<T>> {
        self
    }

    fn is_finished(&self) -> bool {
        true
    }
}
