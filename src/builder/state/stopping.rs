use async_trait::async_trait;
use tokio::sync::oneshot;

use crate::{
    builder::{Builder, BuilderMessage, BuilderMessages},
    error::Error,
};

use super::{BuilderState, Quitting};

pub struct Stopping;

impl Stopping {
    pub(in super::super) fn new_dyn<T>() -> Box<dyn BuilderState<T>>
    where
        T: BuilderMessages + Clone + Sync + Send + 'static,
    {
        Box::new(Self)
    }
}

#[async_trait]
impl<T> BuilderState<T> for Stopping
where
    T: BuilderMessages + Clone + Sync + Send + 'static,
{
    async fn start_build(self: Box<Self>, _builder: &Builder<T>) -> Box<dyn BuilderState<T>> {
        self
    }

    async fn command_completed(
        self: Box<Self>,
        builder: &Builder<T>,
        _stdout: String,
        _stderr: String,
    ) -> Box<dyn BuilderState<T>> {
        let _ = builder.tx.send(BuilderMessage::NextCommand);
        self
    }

    async fn command_failed(
        self: Box<Self>,
        _builder: &Builder<T>,
        _error: Error,
    ) -> Box<dyn BuilderState<T>> {
        self
    }

    async fn next_command(self: Box<Self>, builder: &Builder<T>) -> Box<dyn BuilderState<T>> {
        builder
            .run_next_command(Some(builder.commands.clone().into_iter()), Some(Vec::new()))
            .await
    }

    async fn quit(
        self: Box<Self>,
        _builder: &Builder<T>,
        sender: oneshot::Sender<()>,
    ) -> Box<dyn BuilderState<T>> {
        Quitting::new_dyn(sender)
    }
}
