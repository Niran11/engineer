use async_trait::async_trait;
use tokio::sync::oneshot;

use crate::{
    builder::{Builder, BuilderMessages},
    error::Error,
};

use super::{BuilderState, Quitted};

pub struct Quitting(Option<oneshot::Sender<()>>);

impl Quitting {
    pub(in super::super) fn new_dyn<T>(sender: oneshot::Sender<()>) -> Box<dyn BuilderState<T>>
    where
        T: BuilderMessages + Clone + Sync + Send + 'static,
    {
        Box::new(Self(Some(sender)))
    }
}

#[async_trait]
impl<T> BuilderState<T> for Quitting
where
    T: BuilderMessages + Clone + Sync + Send + 'static,
{
    async fn start_build(mut self: Box<Self>, _builder: &Builder<T>) -> Box<dyn BuilderState<T>> {
        self
    }

    async fn command_completed(
        mut self: Box<Self>,
        _builder: &Builder<T>,
        _stdout: String,
        _stderr: String,
    ) -> Box<dyn BuilderState<T>> {
        self.respond_and_quit()
    }

    async fn command_failed(
        mut self: Box<Self>,
        _builder: &Builder<T>,
        _error: Error,
    ) -> Box<dyn BuilderState<T>> {
        self.respond_and_quit()
    }

    async fn next_command(mut self: Box<Self>, _builder: &Builder<T>) -> Box<dyn BuilderState<T>> {
        self.respond_and_quit()
    }

    async fn quit(
        mut self: Box<Self>,
        _builder: &Builder<T>,
        sender: oneshot::Sender<()>,
    ) -> Box<dyn BuilderState<T>> {
        Self::new_dyn(sender)
    }
}

impl Quitting {
    fn respond_and_quit<T>(&mut self) -> Box<dyn BuilderState<T>>
    where
        T: BuilderMessages + Clone + Sync + Send + 'static,
    {
        self.0.take().map(|tx| tx.send(()));
        Quitted::new_dyn()
    }
}
