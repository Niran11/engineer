use async_trait::async_trait;
use tokio::sync::oneshot;

use super::{Builder, BuilderMessages};
use crate::error::Error;

mod building;
mod quitted;
mod quitting;
mod stopping;
mod waiting;

pub(super) use building::Building;
pub(super) use quitted::Quitted;
pub(super) use quitting::Quitting;
pub(super) use stopping::Stopping;
pub(super) use waiting::Waiting;

#[async_trait]
pub(super) trait BuilderState<T>: Sync + Send
where
    T: BuilderMessages + Clone + Sync + Send + 'static,
{
    async fn start_build(self: Box<Self>, builder: &Builder<T>) -> Box<dyn BuilderState<T>>;

    async fn command_completed(
        self: Box<Self>,
        builder: &Builder<T>,
        stdout: String,
        stderr: String,
    ) -> Box<dyn BuilderState<T>>;

    async fn command_failed(
        self: Box<Self>,
        builder: &Builder<T>,
        error: Error,
    ) -> Box<dyn BuilderState<T>>;

    async fn next_command(self: Box<Self>, builder: &Builder<T>) -> Box<dyn BuilderState<T>>;

    async fn quit(
        self: Box<Self>,
        builder: &Builder<T>,
        sender: oneshot::Sender<()>,
    ) -> Box<dyn BuilderState<T>>;

    fn is_finished(&self) -> bool {
        false
    }
}
