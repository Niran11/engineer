use async_trait::async_trait;
use std::vec::IntoIter;
use tokio::sync::oneshot;

use crate::{
    builder::{Builder, BuilderMessage, BuilderMessages},
    command_executor::CommandExecutorHandle,
    config::KillOption,
    error::Error,
};

use super::{BuilderState, Quitting, Stopping, Waiting};

pub struct Building {
    cmd_executor: CommandExecutorHandle,
    outs: Option<Vec<(String, String)>>,
    commands: Option<IntoIter<String>>,
}

impl Building {
    pub(in super::super) fn new_dyn<T>(
        cmd_executor: CommandExecutorHandle,
        outs: Option<Vec<(String, String)>>,
        commands: Option<IntoIter<String>>,
    ) -> Box<dyn BuilderState<T>>
    where
        T: BuilderMessages + Clone + Sync + Send + 'static,
    {
        Box::new(Self {
            cmd_executor,
            outs,
            commands,
        })
    }
}

#[async_trait]
impl<T> BuilderState<T> for Building
where
    T: BuilderMessages + Clone + Sync + Send + 'static,
{
    async fn start_build(self: Box<Self>, _builder: &Builder<T>) -> Box<dyn BuilderState<T>> {
        self.cmd_executor.kill(KillOption::None);
        Stopping::new_dyn()
    }

    async fn command_completed(
        mut self: Box<Self>,
        builder: &Builder<T>,
        stdout: String,
        stderr: String,
    ) -> Box<dyn BuilderState<T>> {
        if let Some(outs) = &mut self.outs {
            outs.push((stdout, stderr));
        }
        let _ = builder.tx.send(BuilderMessage::NextCommand);
        self
    }

    async fn command_failed(
        mut self: Box<Self>,
        builder: &Builder<T>,
        error: Error,
    ) -> Box<dyn BuilderState<T>> {
        builder.notifier.build_failed(
            builder.build_name.clone(),
            error,
            self.outs.unwrap_or_default(),
        );
        Waiting::new_dyn()
    }

    async fn next_command(mut self: Box<Self>, builder: &Builder<T>) -> Box<dyn BuilderState<T>> {
        builder.run_next_command(self.commands, self.outs).await
    }

    async fn quit(
        self: Box<Self>,
        _builder: &Builder<T>,
        sender: oneshot::Sender<()>,
    ) -> Box<dyn BuilderState<T>> {
        self.cmd_executor.kill(KillOption::None);
        Quitting::new_dyn(sender)
    }
}
