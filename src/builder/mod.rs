use std::vec::IntoIter;

use log::{info, trace};
use tokio::sync::{
    mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender},
    oneshot,
};

use crate::{
    command_executor::{CommandExecutor, CommandExecutorMessages},
    config::Build,
    error::Error,
    file_watcher::FileWatcher,
    utils::BuildName,
};

mod state;

use state::BuilderState;

pub struct Builder<T: BuilderMessages + Clone + Sync + Send + 'static> {
    rx: UnboundedReceiver<BuilderMessage>,
    tx: UnboundedSender<BuilderMessage>,
    _watcher: Option<FileWatcher>,
    build_name: BuildName,
    env: Vec<(String, String)>,
    commands: Vec<String>,
    state: Option<Box<dyn BuilderState<T>>>,
    notifier: T,
}

pub struct BuilderHandle(UnboundedSender<BuilderMessage>);

enum BuilderMessage {
    StartBuild,
    CommandCompleted { stdout: String, stderr: String },
    CommandFailed { error: Error },
    NextCommand,
    Quit(oneshot::Sender<()>),
}

pub trait BuilderMessages {
    fn build_completed(&self, build_name: BuildName);
    fn build_failed(&self, build_name: BuildName, error: Error, outs: Vec<(String, String)>);
    fn file_changed(&self, build_name: BuildName);
}

impl<T> Builder<T>
where
    T: BuilderMessages + Clone + Sync + Send + 'static,
{
    pub async fn start<'a>(
        build_name: BuildName,
        build: &'a Build,
        global_env: impl Iterator<Item = (&'a String, &'a String)>,
        notifier: T,
    ) -> Result<BuilderHandle, Error> {
        let watcher = if build.watch.is_empty() {
            None
        } else {
            let w = FileWatcher::new(build_name.clone(), &build.watch, notifier.clone())?;
            Some(w)
        };
        let (tx, rx) = unbounded_channel();
        let me = Self {
            rx,
            tx: tx.clone(),
            _watcher: watcher,
            build_name,
            env: global_env
                .chain(build.env.iter())
                .map(|(n, v)| (n.to_owned(), v.to_owned()))
                .collect(),
            commands: build.commands.clone(),
            state: Some(state::Waiting::new_dyn()),
            notifier,
        };
        tokio::spawn(async move { me.run().await });
        Ok(BuilderHandle(tx))
    }

    async fn run(mut self) {
        while let Some(msg) = self.rx.recv().await {
            let new_state = match msg {
                BuilderMessage::StartBuild => {
                    info!("Rebuilding {:?}", self.build_name);
                    self.take_state().start_build(&self).await
                }
                BuilderMessage::CommandCompleted { stdout, stderr } => {
                    self.take_state()
                        .command_completed(&self, stdout, stderr)
                        .await
                }
                BuilderMessage::CommandFailed { error } => {
                    self.take_state().command_failed(&self, error).await
                }
                BuilderMessage::NextCommand => self.take_state().next_command(&self).await,
                BuilderMessage::Quit(tx) => self.take_state().quit(&self, tx).await,
            };
            if new_state.is_finished() {
                break;
            }
            self.state = Some(new_state);
        }
    }

    fn take_state(&mut self) -> Box<dyn BuilderState<T>> {
        self.state
            .take()
            .unwrap_or_else(|| state::Waiting::new_dyn())
    }

    async fn run_next_command(
        &self,
        mut commands: Option<IntoIter<String>>,
        outs: Option<Vec<(String, String)>>,
    ) -> Box<dyn BuilderState<T>> {
        trace!("build {} is running next command", self.build_name);
        if let Some(cmd) = commands.as_mut().and_then(|cmd| cmd.next()) {
            match CommandExecutor::execute(&self.env, &cmd, self.tx.clone()).await {
                Ok(cmd_executor) => state::Building::new_dyn(cmd_executor, outs, commands),
                Err(e) => {
                    self.notifier
                        .build_failed(self.build_name.clone(), e, Vec::new());
                    state::Waiting::new_dyn()
                }
            }
        } else {
            self.notifier.build_completed(self.build_name.clone());
            state::Waiting::new_dyn()
        }
    }
}

impl BuilderHandle {
    pub fn start_build(&self) {
        let _ = self.0.send(BuilderMessage::StartBuild);
    }

    pub fn quit(&self, tx: oneshot::Sender<()>) {
        let _ = self.0.send(BuilderMessage::Quit(tx));
    }
}

impl CommandExecutorMessages for UnboundedSender<BuilderMessage> {
    fn command_finished(&self, stdout: String, stderr: String) {
        let _ = self.send(BuilderMessage::CommandCompleted { stdout, stderr });
    }

    fn command_crashed(&self, error: Error) {
        let _ = self.send(BuilderMessage::CommandFailed { error });
    }
}
