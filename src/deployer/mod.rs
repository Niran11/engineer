use std::vec::IntoIter;

use log::{info, trace};
use tokio::sync::{
    mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender},
    oneshot,
};

use crate::{
    command_executor::{CommandExecutor, CommandExecutorMessages},
    config::{Deploy, KillOption},
    error::Error,
    utils::DeployName,
};

mod state;

use state::DeployerState;

pub struct Deployer<T: DeployerMessages + Sync + Send + 'static> {
    rx: UnboundedReceiver<DeployerMessage>,
    tx: UnboundedSender<DeployerMessage>,
    deploy_name: DeployName,
    env: Vec<(String, String)>,
    commands: Vec<String>,
    kill: KillOption,
    state: Option<Box<dyn DeployerState<T>>>,
    notifier: T,
}

pub struct DeployerHandle(UnboundedSender<DeployerMessage>);

enum DeployerMessage {
    Restart,
    CommandCompleted { stdout: String, stderr: String },
    CommandFailed { error: Error },
    NextCommand,
    Quit(oneshot::Sender<()>),
}

pub trait DeployerMessages {
    fn deploy_failed(&self, deploy_name: DeployName, error: Error, outs: Vec<(String, String)>);
}

impl<T> Deployer<T>
where
    T: DeployerMessages + Sync + Send + 'static,
{
    pub async fn start<'a>(
        deploy_name: DeployName,
        deploy: &'a Deploy,
        global_env: impl Iterator<Item = (&'a String, &'a String)>,
        notifier: T,
    ) -> Result<DeployerHandle, Error> {
        let (tx, rx) = unbounded_channel();
        let me = Self {
            rx,
            tx: tx.clone(),
            deploy_name,
            env: global_env
                .chain(deploy.env.iter())
                .map(|(n, v)| (n.clone(), v.clone()))
                .collect(),
            commands: deploy.commands.clone(),
            kill: deploy.kill.clone(),
            state: Some(state::Waiting::new_dyn()),
            notifier,
        };
        tokio::spawn(async move { me.run().await });
        Ok(DeployerHandle(tx))
    }

    async fn run(mut self) {
        while let Some(msg) = self.rx.recv().await {
            let new_state = match msg {
                DeployerMessage::Restart => {
                    info!("Restarting {:?}", self.deploy_name);
                    self.take_state().restart(&self).await
                }
                DeployerMessage::CommandCompleted { stdout, stderr } => {
                    self.take_state()
                        .command_completed(&self, stdout, stderr)
                        .await
                }
                DeployerMessage::CommandFailed { error } => {
                    self.take_state().command_failed(&self, error).await
                }
                DeployerMessage::NextCommand => self.take_state().next_command(&self).await,
                DeployerMessage::Quit(tx) => self.take_state().quit(&self, tx).await,
            };
            if new_state.is_finished() {
                break;
            }
            self.state = Some(new_state);
        }
    }

    fn take_state(&mut self) -> Box<dyn DeployerState<T>> {
        self.state
            .take()
            .unwrap_or_else(|| state::Waiting::new_dyn())
    }

    async fn run_next_command(
        &self,
        mut commands: Option<IntoIter<String>>,
        outs: Option<Vec<(String, String)>>,
    ) -> Box<dyn DeployerState<T>> {
        trace!("deploy {} is running next command", self.deploy_name);
        if let Some(cmd) = commands.as_mut().and_then(|cmd| cmd.next()) {
            match CommandExecutor::execute(&self.env, &cmd, self.tx.clone()).await {
                Ok(cmd_executor) => state::Deploying::new_dyn(cmd_executor, outs, commands),
                Err(e) => {
                    self.notifier.deploy_failed(
                        self.deploy_name.clone(),
                        e,
                        outs.unwrap_or_default(),
                    );
                    state::Waiting::new_dyn()
                }
            }
        } else {
            state::Waiting::new_dyn()
        }
    }
}

impl DeployerHandle {
    pub fn restart_deploy(&self) {
        let _ = self.0.send(DeployerMessage::Restart);
    }

    pub fn quit(&self, tx: oneshot::Sender<()>) {
        let _ = self.0.send(DeployerMessage::Quit(tx));
    }
}

impl CommandExecutorMessages for UnboundedSender<DeployerMessage> {
    fn command_finished(&self, stdout: String, stderr: String) {
        let _ = self.send(DeployerMessage::CommandCompleted { stdout, stderr });
    }

    fn command_crashed(&self, error: Error) {
        let _ = self.send(DeployerMessage::CommandFailed { error });
    }
}
