use async_trait::async_trait;
use tokio::sync::oneshot;

use crate::{
    deployer::{Deployer, DeployerMessages},
    error::Error,
};

use super::DeployerState;

pub struct Quitted;

impl Quitted {
    pub(in super::super) fn new_dyn<T>() -> Box<dyn DeployerState<T>>
    where
        T: DeployerMessages + Sync + Send + 'static,
    {
        Box::new(Self)
    }
}

#[async_trait]
impl<T> DeployerState<T> for Quitted
where
    T: DeployerMessages + Sync + Send + 'static,
{
    async fn restart(self: Box<Self>, _deployer: &Deployer<T>) -> Box<dyn DeployerState<T>> {
        self
    }

    async fn command_completed(
        self: Box<Self>,
        _deployer: &Deployer<T>,
        _stdout: String,
        _stderr: String,
    ) -> Box<dyn DeployerState<T>> {
        self
    }

    async fn command_failed(
        self: Box<Self>,
        _deployer: &Deployer<T>,
        _error: Error,
    ) -> Box<dyn DeployerState<T>> {
        self
    }

    async fn next_command(self: Box<Self>, _deployer: &Deployer<T>) -> Box<dyn DeployerState<T>> {
        self
    }

    async fn quit(
        self: Box<Self>,
        _deployer: &Deployer<T>,
        _sender: oneshot::Sender<()>,
    ) -> Box<dyn DeployerState<T>> {
        self
    }

    fn is_finished(&self) -> bool {
        true
    }
}
