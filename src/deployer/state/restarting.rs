use async_trait::async_trait;
use tokio::sync::oneshot;

use crate::{
    deployer::{Deployer, DeployerMessage, DeployerMessages},
    error::Error,
};

use super::{DeployerState, Quitting};

pub struct Restarting;

impl Restarting {
    pub(in super::super) fn new_dyn<T>() -> Box<dyn DeployerState<T>>
    where
        T: DeployerMessages + Sync + Send + 'static,
    {
        Box::new(Self)
    }
}

#[async_trait]
impl<T> DeployerState<T> for Restarting
where
    T: DeployerMessages + Sync + Send + 'static,
{
    async fn restart(self: Box<Self>, _deployer: &Deployer<T>) -> Box<dyn DeployerState<T>> {
        self
    }

    async fn command_completed(
        self: Box<Self>,
        deployer: &Deployer<T>,
        _stdout: String,
        _stderr: String,
    ) -> Box<dyn DeployerState<T>> {
        let _ = deployer.tx.send(DeployerMessage::NextCommand);
        self
    }

    async fn command_failed(
        self: Box<Self>,
        deployer: &Deployer<T>,
        _error: Error,
    ) -> Box<dyn DeployerState<T>> {
        let _ = deployer.tx.send(DeployerMessage::NextCommand);
        self
    }

    async fn next_command(self: Box<Self>, deployer: &Deployer<T>) -> Box<dyn DeployerState<T>> {
        deployer
            .run_next_command(
                Some(deployer.commands.clone().into_iter()),
                Some(Vec::new()),
            )
            .await
    }

    async fn quit(
        self: Box<Self>,
        _deployer: &Deployer<T>,
        sender: oneshot::Sender<()>,
    ) -> Box<dyn DeployerState<T>> {
        Quitting::new_dyn(sender)
    }
}
