use async_trait::async_trait;
use std::vec::IntoIter;
use tokio::sync::oneshot;

use crate::{
    command_executor::CommandExecutorHandle,
    deployer::{Deployer, DeployerMessage, DeployerMessages},
    error::Error,
};

use super::{DeployerState, Quitting, Restarting, Waiting};

pub struct Deploying {
    cmd_executor: CommandExecutorHandle,
    outs: Option<Vec<(String, String)>>,
    commands: Option<IntoIter<String>>,
}

impl Deploying {
    pub(in super::super) fn new_dyn<T>(
        cmd_executor: CommandExecutorHandle,
        outs: Option<Vec<(String, String)>>,
        commands: Option<IntoIter<String>>,
    ) -> Box<dyn DeployerState<T>>
    where
        T: DeployerMessages + Sync + Send + 'static,
    {
        Box::new(Self {
            cmd_executor,
            outs,
            commands,
        })
    }
}

#[async_trait]
impl<T> DeployerState<T> for Deploying
where
    T: DeployerMessages + Sync + Send + 'static,
{
    async fn restart(self: Box<Self>, deployer: &Deployer<T>) -> Box<dyn DeployerState<T>> {
        self.cmd_executor.kill(deployer.kill.clone());
        Restarting::new_dyn()
    }

    async fn command_completed(
        mut self: Box<Self>,
        deployer: &Deployer<T>,
        stdout: String,
        stderr: String,
    ) -> Box<dyn DeployerState<T>> {
        if let Some(outs) = &mut self.outs {
            outs.push((stdout, stderr));
        }
        let _ = deployer.tx.send(DeployerMessage::NextCommand);
        self
    }

    async fn command_failed(
        mut self: Box<Self>,
        deployer: &Deployer<T>,
        error: Error,
    ) -> Box<dyn DeployerState<T>> {
        deployer.notifier.deploy_failed(
            deployer.deploy_name.clone(),
            error,
            self.outs.unwrap_or_default(),
        );
        Waiting::new_dyn()
    }

    async fn next_command(
        mut self: Box<Self>,
        deployer: &Deployer<T>,
    ) -> Box<dyn DeployerState<T>> {
        deployer.run_next_command(self.commands, self.outs).await
    }

    async fn quit(
        self: Box<Self>,
        deployer: &Deployer<T>,
        sender: oneshot::Sender<()>,
    ) -> Box<dyn DeployerState<T>> {
        self.cmd_executor.kill(deployer.kill.clone());
        Quitting::new_dyn(sender)
    }
}
