use async_trait::async_trait;
use tokio::sync::oneshot;

use super::{Deployer, DeployerMessages};
use crate::error::Error;

mod deploying;
mod quitted;
mod quitting;
mod restarting;
mod waiting;

pub(super) use deploying::Deploying;
pub(super) use quitted::Quitted;
pub(super) use quitting::Quitting;
pub(super) use restarting::Restarting;
pub(super) use waiting::Waiting;

#[async_trait]
pub(super) trait DeployerState<T>: Sync + Send
where
    T: DeployerMessages + Sync + Send + 'static,
{
    async fn restart(self: Box<Self>, deployer: &Deployer<T>) -> Box<dyn DeployerState<T>>;

    async fn command_completed(
        self: Box<Self>,
        deployer: &Deployer<T>,
        stdout: String,
        stderr: String,
    ) -> Box<dyn DeployerState<T>>;

    async fn command_failed(
        self: Box<Self>,
        deployer: &Deployer<T>,
        error: Error,
    ) -> Box<dyn DeployerState<T>>;

    async fn next_command(self: Box<Self>, deployer: &Deployer<T>) -> Box<dyn DeployerState<T>>;

    async fn quit(
        self: Box<Self>,
        deployer: &Deployer<T>,
        sender: oneshot::Sender<()>,
    ) -> Box<dyn DeployerState<T>>;

    fn is_finished(&self) -> bool {
        false
    }
}
