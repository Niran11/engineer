use async_trait::async_trait;
use tokio::sync::oneshot;

use crate::{
    deployer::{Deployer, DeployerMessages},
    error::Error,
};

use super::{DeployerState, Quitted};

pub struct Quitting(Option<oneshot::Sender<()>>);

impl Quitting {
    pub(in super::super) fn new_dyn<T>(sender: oneshot::Sender<()>) -> Box<dyn DeployerState<T>>
    where
        T: DeployerMessages + Sync + Send + 'static,
    {
        Box::new(Self(Some(sender)))
    }
}

#[async_trait]
impl<T> DeployerState<T> for Quitting
where
    T: DeployerMessages + Sync + Send + 'static,
{
    async fn restart(mut self: Box<Self>, _deployer: &Deployer<T>) -> Box<dyn DeployerState<T>> {
        self
    }

    async fn command_completed(
        mut self: Box<Self>,
        _deployer: &Deployer<T>,
        _stdout: String,
        _stderr: String,
    ) -> Box<dyn DeployerState<T>> {
        self.respond_and_quit()
    }

    async fn command_failed(
        mut self: Box<Self>,
        _deployer: &Deployer<T>,
        _error: Error,
    ) -> Box<dyn DeployerState<T>> {
        self.respond_and_quit()
    }

    async fn next_command(
        mut self: Box<Self>,
        _deployer: &Deployer<T>,
    ) -> Box<dyn DeployerState<T>> {
        self.respond_and_quit()
    }

    async fn quit(
        mut self: Box<Self>,
        _deployer: &Deployer<T>,
        sender: oneshot::Sender<()>,
    ) -> Box<dyn DeployerState<T>> {
        Self::new_dyn(sender)
    }
}

impl Quitting {
    fn respond_and_quit<T>(&mut self) -> Box<dyn DeployerState<T>>
    where
        T: DeployerMessages + Sync + Send + 'static,
    {
        self.0.take().map(|tx| tx.send(()));
        Quitted::new_dyn()
    }
}
