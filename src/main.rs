mod builder;
mod command_executor;
mod config;
mod dependency_tree;
mod deployer;
mod error;
mod file_watcher;
mod manager;
mod settings;
mod utils;

use error::Error;
use manager::Manager;
use settings::Settings;

use log::{error, info, LevelFilter};

#[tokio::main]
async fn main() {
    match run().await {
        Err(e) => error!("Failed to do work: {}", e),
        Ok(()) => info!("Finally, end of workday!"),
    }
}

async fn run() -> Result<(), Error> {
    let mut builder = env_logger::builder();
    builder.filter_level(LevelFilter::Info);
    builder.parse_default_env();
    builder.init();
    info!("Loading config file");
    let settings = Settings::build()?;
    info!("Starting building");
    Manager::manage(settings).await?;
    Ok(())
}
