use std::{
    process::{Output, Stdio},
    str,
    time::Duration,
};

use command_group::{AsyncCommandGroup, AsyncGroupChild};
use log::{error, info};
use tokio::{
    process::Command,
    sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender},
};

use crate::{config::KillOption, error::Error};

pub struct CommandExecutor {
    child_id: u32,
    env: Vec<(String, String)>,
    rx: UnboundedReceiver<CmdExeMsg>,
    tx: UnboundedSender<CmdExeMsg>,
}

#[derive(Clone)]
pub struct CommandExecutorHandle(UnboundedSender<CmdExeMsg>);

struct CommandWatcher {
    child: AsyncGroupChild,
    command: String,
    tx: UnboundedSender<CmdExeMsg>,
}

enum CmdExeMsg {
    Kill(KillOption),
    ChildExited(Output),
    ChildCrashed(Error),
}

pub trait CommandExecutorMessages {
    fn command_crashed(&self, error: Error);
    fn command_finished(&self, stdout: String, stderr: String);
}

impl CommandExecutor {
    pub async fn execute<T: CommandExecutorMessages + Send + 'static>(
        env: &[(String, String)],
        command: &str,
        notifier: T,
    ) -> Result<CommandExecutorHandle, Error> {
        let child = Self::execute_command(env, command)?;
        let (tx, rx) = unbounded_channel();
        let Some(child_id) = child.id() else {
            notifier.command_finished(String::new(), String::new());
            return Ok(CommandExecutorHandle(tx.clone()));
        };
        CommandWatcher::start(child, command.to_owned(), tx.clone()).await;
        let me = Self {
            child_id,
            env: env.to_vec(),
            rx,
            tx: tx.clone(),
        };
        tokio::spawn(async move {
            me.run(notifier).await;
        });
        Ok(CommandExecutorHandle(tx))
    }

    async fn run<T: CommandExecutorMessages + Send + 'static>(mut self, notifier: T) {
        while let Some(msg) = self.rx.recv().await {
            match msg {
                CmdExeMsg::Kill(KillOption::None) => {
                    Self::send_signal(self.tx.clone(), self.child_id, "SIGINT", Some("SIGTERM"))
                        .await
                }
                CmdExeMsg::Kill(KillOption::Signal(signal)) => {
                    Self::send_signal(self.tx.clone(), self.child_id, &signal, None).await
                }
                CmdExeMsg::Kill(KillOption::Commands(commands)) => {
                    self.run_kill_commands(commands, self.child_id).await
                }
                CmdExeMsg::ChildExited(output) => {
                    notifier.command_finished(
                        str::from_utf8(&output.stdout)
                            .unwrap_or("not text")
                            .to_owned(),
                        str::from_utf8(&output.stderr)
                            .unwrap_or("not text")
                            .to_owned(),
                    );
                    break;
                }
                CmdExeMsg::ChildCrashed(error) => {
                    error!("command crashed, because of: {}", error);
                    notifier.command_crashed(error);
                    break;
                }
            }
        }
    }

    async fn send_signal(
        tx: UnboundedSender<CmdExeMsg>,
        child_id: u32,
        signal: &str,
        next_signal: Option<&str>,
    ) {
        info!("Killing using kill signal: {}", signal);
        let mut cmd = Command::new("kill");
        cmd.args(["-s", signal]).arg(format!("-{}", child_id));
        let result = Self::spawn_command(cmd);
        let signal = next_signal.unwrap_or("SIGKILL").to_owned();
        let wait_seconds = if result.is_err() { 0 } else { 1 };
        tokio::spawn(async move {
            tokio::time::sleep(Duration::from_secs(wait_seconds)).await;
            let _ = tx.send(CmdExeMsg::Kill(KillOption::Signal(signal)));
        });
    }

    async fn run_kill_commands(&self, commands: Vec<String>, child_id: u32) {
        info!("Killing using kill commands");
        let env = self.env.clone();
        let main_tx = self.tx.clone();
        tokio::spawn(async move {
            for cmd in commands.into_iter() {
                match Self::execute_command(&env, cmd) {
                    Ok(mut child) => {
                        let _ = child.wait().await;
                    }
                    Err(error) => {
                        error!("failed to kill, error: {}", error);
                        break;
                    }
                }
            }
            tokio::time::sleep(Duration::from_secs(1)).await;
            Self::send_signal(main_tx, child_id, "SIGKILL", None).await;
        });
    }

    fn execute_command<T: AsRef<str>>(
        env: &[(String, String)],
        command: T,
    ) -> Result<AsyncGroupChild, Error> {
        let mut cmd = Command::new("sh");
        cmd.arg("-c").arg(Self::get_command_string(env, &command));
        Self::spawn_command(cmd)
    }

    fn get_command_string<T: AsRef<str>>(env: &[(String, String)], command: T) -> String {
        let env = env.iter().fold(String::new(), |acc, (name, value)| {
            format!("{}{}=\"{}\" ", acc, name, value)
        });
        if env.is_empty() {
            command.as_ref().to_owned()
        } else {
            format!("export {} && {}", env, command.as_ref())
        }
    }

    fn spawn_command(mut cmd: Command) -> Result<AsyncGroupChild, Error> {
        cmd.stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .group_spawn()
            .map_err(Error::CommandSpawn)
    }
}

impl CommandExecutorHandle {
    pub fn kill(&self, option: KillOption) {
        let _ = self.0.send(CmdExeMsg::Kill(option));
    }
}

impl CommandWatcher {
    async fn start(child: AsyncGroupChild, command: String, tx: UnboundedSender<CmdExeMsg>) {
        tokio::spawn(async move {
            let me = Self { child, command, tx };
            me.watch_process().await;
        });
    }

    async fn watch_process(self) {
        let _ = match self.child.wait_with_output().await {
            Ok(output) => self.tx.send(CmdExeMsg::ChildExited(output)),
            Err(error) => self.tx.send(CmdExeMsg::ChildCrashed(Error::CommandFinish(
                self.command,
                error.kind(),
            ))),
        };
    }
}
