use std::io;

use thiserror::Error;

use crate::utils::BuildName;

#[derive(Error, Debug)]
pub enum Error {
    #[error("I need file with config to watch!")]
    MissingFilename,

    #[error("Failed to read file {0:?} because of error: {1:?}")]
    FileNotExists(String, io::ErrorKind),

    #[error("Could not parse config file because: {0}")]
    ConfigWrongFormat(#[from] toml::de::Error),

    #[error("Config has errors: {0:?}")]
    WrongConfigSettings(Vec<String>),

    #[error("Could not create file watcher: {0}")]
    FileWatcherCreation(#[from] notify::Error),

    #[error("Could not spawn command, because of error: {0}")]
    CommandSpawn(io::Error),

    #[error("Spawned command: {0:?} did not finish properly because of error: {1:?}")]
    CommandFinish(String, io::ErrorKind),

    #[error("Detected that two builds depend on each other: {0} and {1}")]
    CyclomaticDependency(BuildName, BuildName),

    #[error("Could not start listening to signal {0} because of error {1:?}")]
    SignalHook(&'static str, io::ErrorKind),
}
