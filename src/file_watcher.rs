use std::path::Path;

use log::info;
use notify::{INotifyWatcher, RecursiveMode, Watcher};

use crate::{builder::BuilderMessages, error::Error, utils::BuildName};

#[allow(unused)]
pub struct FileWatcher(INotifyWatcher);

impl FileWatcher {
    pub fn new<T: BuilderMessages + Send + 'static>(
        build_name: BuildName,
        paths: &[String],
        notifier: T,
    ) -> Result<FileWatcher, Error> {
        let watcher = Self::setup_watcher(build_name, paths, notifier)?;
        Ok(Self(watcher))
    }

    fn setup_watcher<T: BuilderMessages + Send + 'static>(
        build_name: BuildName,
        paths: &[String],
        notifier: T,
    ) -> Result<INotifyWatcher, Error> {
        info!("Starting watching files of build {build_name}");
        let mut watcher = notify::recommended_watcher(move |_| {
            notifier.file_changed(build_name.clone());
        })?;
        for path in paths.iter() {
            info!("setting up watch for file {path}");
            watcher.watch(Path::new(path), RecursiveMode::Recursive)?;
        }
        Ok(watcher)
    }
}
