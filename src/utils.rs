use serde::Deserialize;
use std::fmt;

#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
#[serde(transparent)]
pub struct BuildName(String);

#[derive(Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
#[serde(transparent)]
pub struct DeployName(String);

pub trait PartName: fmt::Display + fmt::Debug {
    fn type_name() -> &'static str;
}

impl fmt::Display for BuildName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl PartName for BuildName {
    fn type_name() -> &'static str {
        "build"
    }
}

impl PartName for &BuildName {
    fn type_name() -> &'static str {
        "build"
    }
}

impl fmt::Display for DeployName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl PartName for DeployName {
    fn type_name() -> &'static str {
        "deploy"
    }
}
