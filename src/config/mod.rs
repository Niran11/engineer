use std::{
    collections::{HashMap, HashSet},
    fs,
};

use serde::Deserialize;

use crate::{
    error::Error,
    utils::{BuildName, DeployName},
};

mod error;

use error::ConfigError;

#[derive(Deserialize, Debug)]
pub struct Config {
    #[serde(default)]
    pub env: HashMap<String, String>,
    #[serde(default)]
    pub build: HashMap<BuildName, Build>,
    #[serde(default)]
    pub deploy: HashMap<DeployName, Deploy>,
}

#[derive(Deserialize, Debug)]
pub struct Build {
    #[serde(default)]
    pub env: HashMap<String, String>,
    #[serde(default)]
    pub dependencies: HashSet<BuildName>,
    #[serde(default)]
    pub watch: Vec<String>,
    pub commands: Vec<String>,
}

#[derive(Deserialize, Debug)]
pub struct Deploy {
    #[serde(default)]
    pub env: HashMap<String, String>,
    #[serde(default)]
    pub dependencies: HashSet<BuildName>,
    pub commands: Vec<String>,
    #[serde(default)]
    pub kill: KillOption,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum KillOption {
    Signal(String),
    Commands(Vec<String>),
    None,
}

impl Default for KillOption {
    fn default() -> Self {
        Self::None
    }
}

impl Config {
    pub fn build_from_path(path: String) -> Result<Self, Error> {
        let config_path = path.clone();
        let file_content =
            fs::read_to_string(&path).map_err(move |e| Error::FileNotExists(path, e.kind()))?;
        let mut config: Self = toml::from_str(&file_content)?;
        config.check()?;
        config.update_paths(config_path);
        Ok(config)
    }

    fn check(&self) -> Result<(), Error> {
        let mut errors = Vec::new();
        let all_builds_names: HashSet<_> = self.build.keys().collect();
        for (name, deps) in self
            .build
            .iter()
            .map(|(name, b)| (name, &b.dependencies))
            .filter(|(_, d)| d.is_empty())
        {
            if deps.contains(name) {
                errors.push(ConfigError::BuildDependsOnItself(name.clone()))
            }
            for dep in deps.iter() {
                if !all_builds_names.contains(&dep) {
                    errors.push(ConfigError::BuildNonexistentDependency(
                        name.clone(),
                        dep.clone(),
                    ))
                }
            }
        }
        for (name, deps) in self.deploy.iter().map(|(name, b)| (name, &b.dependencies)) {
            for dep in deps.iter() {
                if !all_builds_names.contains(&dep) {
                    errors.push(ConfigError::DeployNonexistentDependency(
                        name.clone(),
                        dep.clone(),
                    ));
                }
            }
        }
        if self.build.is_empty() && self.deploy.is_empty() {
            errors.push(ConfigError::NoBuildOrDeploy);
        }
        if errors.is_empty() {
            Ok(())
        } else {
            let errors = errors.into_iter().map(|e| e.to_string()).collect();
            Err(Error::WrongConfigSettings(errors))
        }
    }

    fn update_paths(&mut self, config_path: String) {
        let config_folder_path = config_path
            .rsplit_once('/')
            .map(|(path, _filename)| path)
            .unwrap_or(".");
        for path in self
            .build
            .values_mut()
            .flat_map(|b| b.watch.iter_mut())
            .filter(|p| !p.starts_with('/'))
        {
            *path = format!("{}/{}", config_folder_path, path)
        }
    }
}
