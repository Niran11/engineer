use thiserror::Error;

use crate::utils::{BuildName, DeployName};

#[derive(Error, Debug)]
pub(super) enum ConfigError {
    #[error("Build {0} is depending on itself")]
    BuildDependsOnItself(BuildName),

    #[error("Build {0} has nonexistent dependency: {1}")]
    BuildNonexistentDependency(BuildName, BuildName),

    #[error("Deploy {0} has nonexistent dependency: {1}")]
    DeployNonexistentDependency(DeployName, BuildName),

    #[error("There is not a single build nor deploy in provided config")]
    NoBuildOrDeploy,
}
