use std::collections::{HashMap, HashSet};

use log::{error, info};
use tokio::{
    signal::unix::{signal, SignalKind},
    sync::{
        mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender},
        oneshot,
    },
};

use crate::{
    builder::{Builder, BuilderHandle},
    dependency_tree::{BuildDependencies, DeployDependencies},
    deployer::{Deployer, DeployerHandle},
    error::Error,
    settings::Settings,
    utils::{BuildName, DeployName, PartName},
};

mod message;
mod state;

use message::ManagerMessage;
use state::ManagerState;

pub struct Manager {
    tx: UnboundedSender<ManagerMessage>,
    rx: UnboundedReceiver<ManagerMessage>,
    settings: Settings,
    builders: HashMap<BuildName, BuilderHandle>,
    deployers: HashMap<DeployName, DeployerHandle>,
    build_dependencies: BuildDependencies,
    deploy_dependencies: DeployDependencies,
    state: Option<Box<dyn ManagerState>>,
}

impl Manager {
    pub async fn manage(settings: Settings) -> Result<(), Error> {
        let (tx, rx) = unbounded_channel();
        Self::setup_signal_handlers(tx.clone()).await?;
        let cfg = settings.config();
        let mut me = Self {
            tx,
            rx,
            builders: HashMap::new(),
            deployers: HashMap::new(),
            build_dependencies: BuildDependencies::new(&cfg.build)?,
            deploy_dependencies: DeployDependencies::new(&cfg.deploy),
            state: Some(state::WaitingForChanges::new_dyn()),
            settings,
        };
        me.start_builders().await?;
        me.start_deployers().await?;
        me.run().await;
        Ok(())
    }

    async fn setup_signal_handlers(tx: UnboundedSender<ManagerMessage>) -> Result<(), Error> {
        let mut sigint =
            signal(SignalKind::interrupt()).map_err(|e| Error::SignalHook("SIGINT", e.kind()))?;
        let sigint_tx = tx.clone();
        tokio::spawn(async move {
            while sigint.recv().await.is_some() {
                let _ = sigint_tx.send(ManagerMessage::Quit);
            }
        });
        let mut sigterm =
            signal(SignalKind::terminate()).map_err(|e| Error::SignalHook("SIGTERM", e.kind()))?;
        let sigterm_tx = tx;
        tokio::spawn(async move {
            while sigterm.recv().await.is_some() {
                let _ = sigterm_tx.send(ManagerMessage::Quit);
            }
        });
        Ok(())
    }

    async fn start_builders(&mut self) -> Result<(), Error> {
        let cfg = self.settings.config();
        for (name, build) in cfg.build.iter() {
            let builder =
                Builder::start(name.clone(), build, cfg.env.iter(), self.tx.clone()).await?;
            self.builders.insert(name.clone(), builder);
        }
        Ok(())
    }

    async fn start_deployers(&mut self) -> Result<(), Error> {
        let cfg = self.settings.config();
        for (name, deploy) in cfg.deploy.iter() {
            let deployer =
                Deployer::start(name.clone(), deploy, cfg.env.iter(), self.tx.clone()).await?;
            self.deployers.insert(name.clone(), deployer);
        }
        Ok(())
    }

    async fn run(mut self) {
        let _ = self.tx.send(ManagerMessage::BuildAll);
        while let Some(msg) = self.rx.recv().await {
            let new_state = match msg {
                ManagerMessage::BuildCompleted(build_name) => {
                    self.take_state().build_completed(&self, build_name).await
                }
                ManagerMessage::BuildFailed(build_name, error, outs) => {
                    self.print_build_error_msg(&build_name, error, outs);
                    self.take_state().build_failed(&self, build_name).await
                }
                ManagerMessage::FileChanged(build_name) => {
                    self.take_state().file_changed(&self, build_name).await
                }

                ManagerMessage::DeployFailed(deploy_name, error, outs) => {
                    self.print_deploy_error_msg(deploy_name, error, outs);
                    self.take_state()
                }
                ManagerMessage::BuildAll => self.take_state().build_all(&self).await,
                ManagerMessage::NextBuildStep => self.take_state().next_build_step(&self).await,
                ManagerMessage::Quit => {
                    let total_len = self.builders.len() + self.deployers.len();
                    let mut rxs = Vec::with_capacity(total_len);
                    let mut channels = (0..total_len).map(|_| oneshot::channel());
                    for (builder, (tx, rx)) in self.builders.values().zip(channels.by_ref()) {
                        builder.quit(tx);
                        rxs.push(rx);
                    }
                    for (deployer, (tx, rx)) in self.deployers.values().zip(channels) {
                        deployer.quit(tx);
                        rxs.push(rx);
                    }
                    for rx in rxs.into_iter() {
                        let _ = rx.await;
                    }
                    break;
                }
            };
            self.state = Some(new_state);
        }
    }

    fn take_state(&mut self) -> Box<dyn ManagerState> {
        self.state
            .take()
            .unwrap_or_else(|| state::WaitingForChanges::new_dyn())
    }

    fn print_build_error_msg(
        &self,
        build_name: &BuildName,
        error: Error,
        outs: Vec<(String, String)>,
    ) {
        let commands = self
            .settings
            .config()
            .build
            .get(build_name)
            .map(|b| &b.commands);
        self.print_error_msg(build_name, error, outs, commands);
    }

    fn print_deploy_error_msg(
        &self,
        deploy_name: DeployName,
        error: Error,
        outs: Vec<(String, String)>,
    ) {
        let commands = self
            .settings
            .config()
            .deploy
            .get(&deploy_name)
            .map(|d| &d.commands);
        self.print_error_msg(deploy_name, error, outs, commands);
    }

    fn print_error_msg<T: PartName>(
        &self,
        part_name: T,
        error: Error,
        outs: Vec<(String, String)>,
        commands: Option<&Vec<String>>,
    ) {
        let mut last_command_idx = -1;
        if let Some(commands) = commands {
            info!("Failed {} {:?} output", T::type_name(), part_name);
            for ((i, cmd), (stdout, stderr)) in commands.iter().enumerate().zip(outs.into_iter()) {
                println!("command: {:?}", cmd);
                println!("--- stdout ---");
                println!("{}", stdout);
                println!();
                println!("--- stderr --");
                println!("{}", stderr);
                println!();
                last_command_idx = i as i64;
            }
            println!(
                "Command {:?} failed with: {}",
                commands[(last_command_idx + 1) as usize],
                error
            );
        } else {
            error!(
                "{} {:?} failed, but it does not exist!",
                T::type_name(),
                part_name
            );
        }
    }

    // TODO: this and next functions should be now able to take &self...
    fn next_build_step(
        &self,
        built: &HashSet<BuildName>,
        failed: &HashSet<BuildName>,
        in_progress: &mut HashSet<BuildName>,
    ) -> bool {
        let mut would_build_something = false;
        for (build_name, builder) in self.builders.iter() {
            if built.contains(build_name) || failed.contains(build_name) {
                continue;
            }
            let depends_failed = failed
                .iter()
                .any(|name| self.build_dependencies.depends(build_name, name));
            if depends_failed {
                continue;
            }
            let depends_directly = built
                .iter()
                .any(|name| self.build_dependencies.depends_directly(build_name, name));
            if !depends_directly {
                continue;
            }
            let depends_indirectly = built
                .iter()
                .any(|name| self.build_dependencies.depends_indirectly(build_name, name));
            if depends_indirectly {
                continue;
            }
            would_build_something = true;
            Self::start_build(build_name, Some(builder), in_progress);
        }
        would_build_something
    }

    fn start_build(
        build_name: &BuildName,
        builder: Option<&BuilderHandle>,
        in_progress: &mut HashSet<BuildName>,
    ) {
        if let Some(builder) = builder {
            builder.start_build();
            in_progress.insert(build_name.clone());
        }
    }

    fn restart_deployments(
        &self,
        built: &HashSet<BuildName>,
        failed: &HashSet<BuildName>,
        deploy_empty: bool,
    ) {
        info!("Restarting deployments");
        for (deploy_name, deployer) in self.deployers.iter() {
            let depends_failed = failed
                .iter()
                .any(|name| self.deploy_dependencies.depends(deploy_name, name));
            if depends_failed {
                continue;
            }
            let depends_built = built
                .iter()
                .any(|name| self.deploy_dependencies.depends(deploy_name, name));
            if depends_built
                || (deploy_empty && !self.deploy_dependencies.has_dependencies(deploy_name))
            {
                deployer.restart_deploy();
            }
        }
    }
}
