use std::collections::HashSet;

use async_trait::async_trait;

use crate::{
    manager::{message::ManagerMessage, Manager},
    utils::BuildName,
};

use super::{ManagerState, WaitingForChanges};

pub(in super::super) struct Building {
    in_progress: HashSet<BuildName>,
    built: HashSet<BuildName>,
    failed: HashSet<BuildName>,
    changed_files: HashSet<BuildName>,
    deploy_empty: bool,
}

impl Building {
    pub fn new_dyn(
        in_progress: HashSet<BuildName>,
        changed_files: HashSet<BuildName>,
        deploy_empty: bool,
    ) -> Box<dyn ManagerState> {
        Box::new(Self {
            in_progress,
            built: HashSet::new(),
            failed: HashSet::new(),
            changed_files,
            deploy_empty,
        })
    }
}

#[async_trait]
impl ManagerState for Building {
    async fn build_completed(
        mut self: Box<Self>,
        manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        Self::update_in_progress(manager, build_name, &mut self.in_progress, &mut self.built);
        self
    }

    async fn build_failed(
        mut self: Box<Self>,
        manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        Self::update_in_progress(manager, build_name, &mut self.in_progress, &mut self.failed);
        self
    }

    async fn file_changed(
        mut self: Box<Self>,
        _manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        self.changed_files.insert(build_name);
        self
    }

    async fn build_all(self: Box<Self>, manager: &Manager) -> Box<dyn ManagerState> {
        Self::new_dyn(
            HashSet::new(),
            manager.build_dependencies.all_no_dependencies(),
            true,
        )
    }

    async fn next_build_step(mut self: Box<Self>, manager: &Manager) -> Box<dyn ManagerState> {
        if self.changed_files.is_empty() {
            if manager.next_build_step(&self.built, &self.failed, &mut self.in_progress) {
                if self.in_progress.is_empty() {
                    WaitingForChanges::new_dyn()
                } else {
                    self
                }
            } else {
                manager.restart_deployments(&self.built, &self.failed, self.deploy_empty);
                WaitingForChanges::new_dyn()
            }
        } else {
            for changed in self.changed_files.drain() {
                self.built
                    .retain(|b| !manager.build_dependencies.depends(b, &changed));
                self.failed
                    .retain(|b| !manager.build_dependencies.depends(b, &changed));
                Manager::start_build(
                    &changed,
                    manager.builders.get(&changed),
                    &mut self.in_progress,
                );
            }
            self
        }
    }
}

impl Building {
    fn update_in_progress(
        manager: &Manager,
        build_name: BuildName,
        in_progress: &mut HashSet<BuildName>,
        to_insert: &mut HashSet<BuildName>,
    ) {
        if in_progress.remove(&build_name) {
            to_insert.insert(build_name);
        }
        if in_progress.is_empty() {
            let _ = manager.tx.send(ManagerMessage::NextBuildStep);
        }
    }
}
