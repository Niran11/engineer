use async_trait::async_trait;

use crate::{manager::Manager, utils::BuildName};

mod building;
mod waiting_for_build;
mod waiting_for_changes;

pub(super) use building::Building;
pub(super) use waiting_for_build::WaitingForBuild;
pub(super) use waiting_for_changes::WaitingForChanges;

#[async_trait]
pub(super) trait ManagerState: Send + Sync {
    async fn build_completed(
        self: Box<Self>,
        manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState>;

    async fn build_failed(
        self: Box<Self>,
        manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState>;

    async fn file_changed(
        self: Box<Self>,
        manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState>;

    async fn build_all(self: Box<Self>, manager: &Manager) -> Box<dyn ManagerState>;

    async fn next_build_step(self: Box<Self>, manager: &Manager) -> Box<dyn ManagerState>;
}
