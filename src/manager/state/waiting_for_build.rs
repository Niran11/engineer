use std::collections::HashSet;

use async_trait::async_trait;

use crate::{
    manager::{message::ManagerMessage, Manager},
    utils::BuildName,
};

use super::{Building, ManagerState};

pub(in super::super) struct WaitingForBuild {
    changed_files: HashSet<BuildName>,
    deploy_empty: bool,
}

impl WaitingForBuild {
    pub fn new_dyn(changed_files: HashSet<BuildName>, deploy_empty: bool) -> Box<dyn ManagerState> {
        Box::new(Self {
            changed_files,
            deploy_empty,
        })
    }
}

#[async_trait]
impl ManagerState for WaitingForBuild {
    async fn build_completed(
        mut self: Box<Self>,
        _manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        self.changed_files.insert(build_name);
        self
    }

    async fn build_failed(
        self: Box<Self>,
        _manager: &Manager,
        _build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        self
    }

    async fn file_changed(
        mut self: Box<Self>,
        _manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        self.changed_files.insert(build_name);
        self
    }

    async fn build_all(mut self: Box<Self>, manager: &Manager) -> Box<dyn ManagerState> {
        self.changed_files = manager.build_dependencies.all_no_dependencies();
        self
    }

    async fn next_build_step(mut self: Box<Self>, manager: &Manager) -> Box<dyn ManagerState> {
        let mut in_progress = HashSet::new();
        for changed in self.changed_files.drain() {
            Manager::start_build(&changed, manager.builders.get(&changed), &mut in_progress);
        }
        if in_progress.is_empty() {
            let _ = manager.tx.send(ManagerMessage::NextBuildStep);
        }
        Building::new_dyn(in_progress, HashSet::new(), self.deploy_empty)
    }
}
