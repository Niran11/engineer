use std::time::Duration;

use async_trait::async_trait;

use crate::{
    manager::{message::ManagerMessage, Manager},
    utils::BuildName,
};

use super::{ManagerState, WaitingForBuild};

pub(in super::super) struct WaitingForChanges;

impl WaitingForChanges {
    pub fn new_dyn() -> Box<dyn ManagerState> {
        Box::new(Self)
    }
}

#[async_trait]
impl ManagerState for WaitingForChanges {
    async fn build_completed(
        self: Box<Self>,
        _manager: &Manager,
        _build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        self
    }

    async fn build_failed(
        self: Box<Self>,
        _manager: &Manager,
        _build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        self
    }

    async fn file_changed(
        self: Box<Self>,
        manager: &Manager,
        build_name: BuildName,
    ) -> Box<dyn ManagerState> {
        let tx = manager.tx.clone();
        tokio::spawn(async move {
            tokio::time::sleep(Duration::from_millis(150)).await;
            let _ = tx.send(ManagerMessage::NextBuildStep);
        });
        WaitingForBuild::new_dyn([build_name].into(), false)
    }

    async fn build_all(self: Box<Self>, manager: &Manager) -> Box<dyn ManagerState> {
        let _ = manager.tx.send(ManagerMessage::NextBuildStep);
        WaitingForBuild::new_dyn(manager.build_dependencies.all_no_dependencies(), true)
    }

    async fn next_build_step(self: Box<Self>, _manager: &Manager) -> Box<dyn ManagerState> {
        self
    }
}
