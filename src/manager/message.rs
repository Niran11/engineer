use tokio::sync::mpsc::UnboundedSender;

use crate::{
    builder::BuilderMessages,
    deployer::DeployerMessages,
    error::Error,
    utils::{BuildName, DeployName},
};

pub(super) enum ManagerMessage {
    BuildCompleted(BuildName),
    BuildFailed(BuildName, Error, Vec<(String, String)>),
    FileChanged(BuildName),

    DeployFailed(DeployName, Error, Vec<(String, String)>),

    BuildAll,
    NextBuildStep,
    Quit,
}

impl BuilderMessages for UnboundedSender<ManagerMessage> {
    fn build_completed(&self, build_name: BuildName) {
        let _ = self.send(ManagerMessage::BuildCompleted(build_name));
    }

    fn build_failed(&self, build_name: BuildName, error: Error, outs: Vec<(String, String)>) {
        let _ = self.send(ManagerMessage::BuildFailed(build_name, error, outs));
    }

    fn file_changed(&self, build_name: BuildName) {
        let _ = self.send(ManagerMessage::FileChanged(build_name));
    }
}

impl DeployerMessages for UnboundedSender<ManagerMessage> {
    fn deploy_failed(&self, deploy_name: DeployName, error: Error, outs: Vec<(String, String)>) {
        let _ = self.send(ManagerMessage::DeployFailed(deploy_name, error, outs));
    }
}
