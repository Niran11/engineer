# Engineer

Build and redeploy application only when it's built successfully.

## Building / Running
`cargo build` and/or `cargo run -- <config_file.toml>`

## How does it work
1. create `toml` file containining commands to run on build & deploy
2. run program giving that `toml` file as argument
3. now `engineer` will build all builds dependent on watched files that changed
  when starting it will build all builds& deployments without dependencies too.
4. after they are built it will restart deployments dependent on these builds
5. deployments are restarted only if builds for it succeeded
6. if build or deployment fails - it shows output of all commands and error from last one
  and does not build/deploys anything dependent on failed builds

## Example config
Only required section in builds and deployments are commands
```toml
# environmental variables to be available in all builds and deployments
[env]
ENV1 = "env1 global"
ENV2 = "env2 global"
ENV3 = "env3 global"

# [build.<name>], name is basically identifier for given build
[build.first]
# environmental variables available only in this build
# these have priority over global ones above
env = { ENV1 = "env1 first", ENV4 = "env4 first", ENV5 = "env5 first" }
# files to be watched by this build
# when any of these changes - build rebuilds itself
# pathes are relative to config file location
# unless they start with "/"
watch = [
  "test_file.txt"
]
# shell commands to be run for this build
commands = [
  "echo first build",
  "echo first env1: $ENV1, env2: $ENV2, env3: $ENV3, env4: $ENV4, env5: $ENV5, env6: $ENV6, env7: $ENV7"
]

[build.second]
env = { ENV1 = "env1 second", ENV6 = "env6 second" }
# what builds it depends on
# they don't need to be above/written before.
# build is rebuilt if any of dependencies gets rebuilt
# also - build can watch files and depend on builds at once
# in such case it gets rebuilt when file changes or when dependency gets built
dependencies = [
  "first"
]
commands = [
  "echo second build",
  "echo second env1: $ENV1, env2: $ENV2, env3: $ENV3, env4: $ENV4, env5: $ENV5, env6: $ENV6, env7: $ENV7"
]

# same as build - deploy.<name>, name is an identifier
[deploy.first]
# builds it depends on
# if any of these builds rebuilds - deployment gets restarted
# but if any of them fails - deploy stays in old version
dependencies = [
  "second"
]
env = { ENV1 = "env1 deploy first", ENV7 = "env7 deploy first" }
commands = [
  "echo first deploy",
  "echo first deploy env1: $ENV1, env2: $ENV2, env3: $ENV3, env4: $ENV4, env5: $ENV5, env6: $ENV6, env7: $ENV7"
]
# Whatever kill command can work with, this is sent to running deployment on restart
# if kill is a list of commands - they are executed in order eg.
# kill = ["echo 1", "echo 2", "kill $(ps aux | grep fancy_app | awk '{ print $2 }')"]
# then SIGKILL is used if deployment does not die
# if this is just a string - given signal is used, then SIGKILL
# by default: SIGINT -> SIGTERM -> SIGKILL.
kill = "SIGTERM"
```